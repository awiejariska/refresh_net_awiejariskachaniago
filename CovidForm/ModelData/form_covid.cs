//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ModelData
{
    using System;
    using System.Collections.Generic;
    
    public partial class form_covid
    {
        public long id { get; set; }
        public bool is_delete { get; set; }
        public string email { get; set; }
        public string nama { get; set; }
        public long npm { get; set; }
        public long umur { get; set; }
        public long jk { get; set; }
        public string telp { get; set; }
        public string alamat { get; set; }
        public string asal { get; set; }
        public string berada { get; set; }
    }
}
