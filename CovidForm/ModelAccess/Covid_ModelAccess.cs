﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelData;
using ModelView;

namespace ModelAccess
{
    public class Covid_ModelAccess
    {
        public static string Message;
        public static List<Covid_ModelView> GetListAll()
        {
            List<Covid_ModelView> listData = new List<Covid_ModelView>();

            using (var db = new refresh_dbEntities())
            {
                listData = (from a in db.form_covid
                            join b in db.jenis_kelamin
                            on a.jk equals b.id
                            where a.is_delete == false
                            select new Covid_ModelView
                            {
                                id = a.id,
                                is_delete = a.is_delete,
                                email = a.email,
                                nama = a.nama,
                                npm = a.npm,
                                umur = a.umur,
                                jk = a.jk,
                                jenisKelamin = b.name,
                                telp = a.telp,
                                alamat = a.alamat,
                                asal = a.asal,
                                berada = a.berada
                            }).ToList();
            }
            return listData;
        }

        public static List<JKModelView> getJKId()
        {
            List<JKModelView> listJK = new List<JKModelView>();
            using (refresh_dbEntities db = new refresh_dbEntities())
            {
                var db_JK = db.jenis_kelamin.Where(a => a.is_delete == false).ToList();
                if (db_JK != null)
                {
                    listJK = db_JK.Select(a => new JKModelView
                    {
                        name = a.name,
                        value = a.id
                    }).ToList();
                }

            }
            return listJK;
        }

        public static bool Insert(Covid_ModelView paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new refresh_dbEntities())
                {
                    form_covid a = new form_covid();
                    a.is_delete = paramModel.is_delete;
                    a.email = paramModel.email;
                    a.nama = paramModel.nama;
                    a.npm = paramModel.npm;
                    a.umur = paramModel.umur;
                    a.jk = paramModel.jk;
                    a.telp = paramModel.telp;
                    a.alamat = paramModel.alamat;
                    a.asal = paramModel.asal;
                    a.berada = paramModel.berada;

                    db.form_covid.Add(a);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        //Mengedit data berdasarkan primary key menggunakan parameter
        public static Covid_ModelView GetDetailsById(int paramId)
        {
            //penampung datanya
            Covid_ModelView result = new Covid_ModelView();

            using (var db = new refresh_dbEntities())
            {
                result = (from a in db.form_covid
                          where a.id == paramId
                          select new Covid_ModelView
                          {
                              id = a.id,
                              is_delete = a.is_delete,
                              email = a.email,
                              nama = a.nama,
                              npm = a.npm,
                              umur = a.umur,
                              jk = a.jk,
                              telp = a.telp,
                              alamat = a.alamat,
                              asal = a.asal,
                              berada = a.berada
                          }).FirstOrDefault();
            }
            return result;
        }

        //Update data
        public static bool Update(Covid_ModelView paramModel)
        {
            //penampung return
            bool result = true;

            try
            {
                using (var db = new refresh_dbEntities())
                {
                    //Mengambil data lama
                   form_covid a = db.form_covid.Where(o => o.id == paramModel.id).FirstOrDefault();

                    if (a != null)
                    {
                        a.is_delete = paramModel.is_delete;
                        a.email = paramModel.email;
                        a.nama = paramModel.nama;
                        a.npm = paramModel.npm;
                        a.umur = paramModel.umur;
                        a.jk = paramModel.jk;
                        a.telp = paramModel.telp;
                        a.alamat = paramModel.alamat;
                        a.asal = paramModel.asal;
                        a.berada = paramModel.berada;

                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static bool Delete(Covid_ModelView paramModelDelete)
        {
            //variabel penampung status
            bool result = true;

            try
            {
                using (var db = new refresh_dbEntities())
                {
                    //Memanggil tabel
                    form_covid a = db.form_covid.Where(o => o.id == paramModelDelete.id).FirstOrDefault();
                    if (a != null)
                    {
                        a.is_delete = true;

                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }
    }
}
