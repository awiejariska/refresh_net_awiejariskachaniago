﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelView
{
    public class Jenis_KelaminModelView
    {
        public long id { get; set; }
        public bool is_delete { get; set; }
        public string name { get; set; }
    }
}
