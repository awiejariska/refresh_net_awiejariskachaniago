﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelView
{
    public class Covid_ModelView
    {
        public long id { get; set; }
        public bool is_delete { get; set; }
        public string email { get; set; }
        public string nama { get; set; }
        public long npm { get; set; }
        public long umur { get; set; }
        public long jk { get; set; }
        public string telp { get; set; }
        public string alamat { get; set; }
        public string asal { get; set; }
        public string berada { get; set; }
        public string jenisKelamin { get; set; }
    }
}
