﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelView;
using CovidForm.Models.Shared;
using ModelAccess;

namespace CovidForm.Controllers
{
    public class CovidController : Controller
    {
        // GET: Covid
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            List<Covid_ModelView> List = new List<Covid_ModelView>();
            List = Covid_ModelAccess.GetListAll();
            return PartialView("List", List);
        }

        public ActionResult Create()
        {
            ViewBag.ListJK = Covid_ModelAccess.getJKId();
            return PartialView();
        }

        [HttpPost]
        public ActionResult Create(Covid_ModelView paramModel)
        {
            try
            {
                paramModel.is_delete = false;

                if (ModelState.IsValid)
                {
                    return Json(new
                    {
                        success = Covid_ModelAccess.Insert(paramModel),
                        message = Covid_ModelAccess.Message
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    List<Message> listMessage =
                     Message.GetMessageErrorByModel("paramModel", ModelState);

                    return Json(new
                    {
                        success = false,
                        message = listMessage
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
            {
                return Json(new
                {
                    success = false,
                    message = hasError.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Edit(Int32 paramId)
        {
            ViewBag.ListJK = Covid_ModelAccess.getJKId();
            return PartialView(Covid_ModelAccess.GetDetailsById(paramId));
        }

        [HttpPost]
        public ActionResult Edit(Covid_ModelView paramModel)
        {
            try
            {

                return Json(new
                {
                    success = Covid_ModelAccess.Update(paramModel),
                    message = Covid_ModelAccess.Message
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception hasError)
            {
                return Json(new
                {
                    success = false,
                    message = hasError.Message
                },
                JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Delete(Int32 IdDelete)
        {
            return PartialView(Covid_ModelAccess.GetDetailsById(IdDelete));
        }

        [HttpPost]
        public ActionResult Delete(Covid_ModelView paramModelDelete)
        {
            try
            {
                paramModelDelete.is_delete = true;

                return Json(new
                {
                    success = Covid_ModelAccess.Delete(paramModelDelete),
                    message = Covid_ModelAccess.Message
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception hasError)
            {
                return Json(new
                {
                    success = false,
                    message = hasError.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}